import './logo.css';
import LogoCtrl from './logo.controller';

const logo = {
    template: require('./logo.html'),
    bindings: {
        text: '<'
    },
    controller: LogoCtrl
};

export default logo;
