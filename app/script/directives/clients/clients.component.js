import './clients.css';
import ClientsCtrl from './clients.controller';

const clients = {
    template: require('./clients.html'),
    bindings: {
        clientData: '<'
    },
    controller: ClientsCtrl
};

export default clients;
