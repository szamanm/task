export default class clientsCtrl {
    constructor(taskApi) {
        taskApi.getClients(data => {
            this.clients = data._embedded.customers;
        });
    }
}
