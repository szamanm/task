import angular from 'angular';

import TaskCtrl from './task.controller';

import logo from '../directives/logo/logo.component';
import clients from '../directives/clients/clients.component';

import taskApi from '../factory/api.factory';

import oauthConstant from '../constats/oauth.constants';

let task = () => {
    return {
        template: require('./task.html'),
        controller: 'taskCtrl',
        controllerAs: 'ctrl'
    }
};

angular.module('task', [])
    .directive('task', task)
    .controller('taskCtrl', TaskCtrl)
    .component('logo', logo)
    .component('clients', clients)
    .service('taskApi',taskApi)
    .constant('oauthConstant', oauthConstant);
