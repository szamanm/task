const oauthConstant = {
    url : 'https://i1.test-services.nykredit.it/security/oauth2/token',
    // base64: test-clientid:password
    Authorization: 'basic dGVzdC1jbGllbnRpZDpwYXNzd29yZA=='
};

export default oauthConstant;