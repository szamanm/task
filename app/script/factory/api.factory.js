export default class taskApi {
        constructor($http, $q, oauthConstant) {
            this.http = $http;
            this.q = $q;
            this.authorization = false;
            this.oauthConstant = oauthConstant;
            this.OAuthPromise = this.getOauth2Token();
        }

        getClients(resp) {
            this.performWithOauth2Token( _=> {
                this.getClientsWithOauth2Token(resp);
            });
        }

        getClientsWithOauth2Token(resp) {
            this.requestToApi(
                "https://i1.test-services.nykredit.it:443/cem-hackathon-service/customers",
                false,
                resp);
        }

        performWithOauth2Token(functionToRun) {
            if (this.authorization) {
                functionToRun();
            } else {
                this.OAuthPromise.then(functionToRun);
            }
        }

        requestToApi(url, data, resp) {
            return this.http({
                method: 'get',
                url: url,
                data: data,
                headers: {
                    Authorization: this.authorization,
                    'X-Client-Version': '1.0.0'
                }
                }).then( (response) => {
                    resp(response.data);
                }).catch( function(error) {
                    console.error('error with request');
                    console.log(error);
                });
        }

        getOauth2Token() {
            if (this.checkForLocalToken()) return true;

            return this.http({
                method: 'post',
                url: this.oauthConstant.url,
                data: 'grant_type=client_credentials',
                headers: {
                    Authorization: this.oauthConstant.Authorization,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then( (response) => {
                this.authorization = response.data.token_type + ' ' + response.data.access_token;
                this.setLocalToken(this.authorization, response.data.expires_in * 1000);
            }).catch( function(error) {
                console.error('error OAuth2');
                console.log(error);
            });
        }

        checkForLocalToken() {
            const currentDate = new Date();
            if (localStorage && localStorage.getItem('Auth') && localStorage.getItem('expireAt')) {
                if (localStorage.getItem('expireAt') * 1 > currentDate.getTime()) {
                    this.authorization = localStorage.getItem('Auth');
                    return true;
                }
            }
            return false;
        }

        setLocalToken(auth, expires_in) {
            const currentDate = new Date();
            if (localStorage) {
                localStorage.setItem('Auth', auth);
                localStorage.setItem('expireAt', expires_in + currentDate.getTime());
            }
        }
    };
